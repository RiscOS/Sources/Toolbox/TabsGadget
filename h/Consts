/*
 * Copyright (c) 2003, Rik Griffin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/* Tabs:Consts.h		*/
/* Rik Griffin Nov 2003		*/

/* define or not to turn debugging on or off	*/
#ifndef Debug_On
 #define Debug_On		0
#endif

/* level of debugging info sent to Tabs:Debug	*/
/* from 0 (none) to 3 (verbose)			*/
#define Debug_Level 		0

#define Program_Error                0x1b000000
#define TabsGadget_ErrorBase         (Program_Error | 0x821C00)
#define TabsGadget_AllocFailed       (TabsGadget_ErrorBase+0x01)
#define TabsGadget_BadIndex          (TabsGadget_ErrorBase+0x0E)
#define TabsGadget_OnlyWindowsNest   (TabsGadget_ErrorBase+0x14)

#ifdef MemCheck_MEMCHECK
#undef _mem_allocate
#undef _mem_free
#undef _mem_extend

#define _mem_allocate(x) mem_allocate_wrapper(x,__FILE__,__LINE__)
#define _mem_free(x) mem_free_wrapper(x,__FILE__,__LINE__)
#define _mem_extend(x,y) mem_extend_wrapper(x,y,__FILE__,__LINE__)

extern void *mem_allocate_wrapper(int x, char *file, int line);
extern void mem_free_wrapper(void *x, char *file, int line);
extern void *mem_extend_wrapper(void *x, int y, char *file, int line);

#else
#define _mem_allocate(x) mem_allocate(x)
#define _mem_free(x) mem_free(x)
#define _mem_extend(x,y) mem_extend(x,y)
#endif
